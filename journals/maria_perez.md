Jun 26:
    worked on authentication, still trying to figure out the functions but we have the server running
Jun 27:
    fixed authentication errors & moved models around
Jun 28:
    Started working on routes
Jun 29:
    Completed update_review route & started working on get_chef route
Jun 30:
    Finished get_chef route, Worked on completing the backend together
Jul 10:
    Started working on frontend pages
Jul 11:
    Finished bare bones of the main jsx pages
Jul 12:
    Starting adding css to main pages while teammates completed the last 2 pages
Jul 13:
    Started adding animations as well as more css & bootstrap classes, cleaning up code
Jul 14:
    completed about and faq page and css for both of em
Jul 15:
    merge conflicts messed with the css so had to start redoing that
Jul 17:
    fixed errors from friday and are back on track to finish the project later this week or next week
Jul 18:
    Added a link to recipe detail page from the recipe name and added css to detail page
Jul 19:
    Started working on finalizing about & faq page
Jul 20:
    Finished both pages styling
Jul 24:
    Started working on finalizing recipe page & my favorites styling
Jul 25:
    Started working on trying to get reviews/ update them specifically
Jul 26:
    Finished recipe page & favorites styling
Jul 27:
    Fininshed main page styling so now they're all done, fixed dropdown nav, & deleted all unused code (cleanign up code)
Jul 28:
    We over anything that was left to do and submitted the project
