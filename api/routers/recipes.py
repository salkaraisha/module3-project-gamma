from fastapi import APIRouter, Depends
from models import RecipesList, RecipesOut
from queries.recipes import RecipesQueries
from typing import List

router = APIRouter()


@router.get("/api/recipes/", response_model=RecipesList)
def list_recipes(queries: RecipesQueries = Depends()):
    return {"recipes": queries.list_recipes()}


@router.get("/api/recipes/{recipe_id}", response_model=RecipesOut)
def get_recipe_by_recipe_id(
    recipe_id: int, queries: RecipesQueries = Depends()
):
    return queries.get_one_by_recipe_id(recipe_id)


@router.get("/api/chefs/{recipe_id}/credits", response_model=List[str])
def get_credits_names_for_recipe_id(
    recipe_id: int, queries: RecipesQueries = Depends()
):
    recipe = queries.get_one_by_recipe_id(recipe_id)
    if recipe:
        credit_names = [credit["name"] for credit in recipe.get("credits", [])]
        return credit_names
    else:
        return []
