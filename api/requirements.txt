fastapi[all]==0.81.0
jwtdown-fastapi>=0.2.0
pymongo[srv]===4.2.0
uvicorn[standard]==0.17.6
requests==2.31.0
pytest
