import requests
import os


class RecipesQueries:
    def list_recipes(self):
        url = "https://tasty.p.rapidapi.com/recipes/list"
        params = {"from": "60", "size": "40"}

        headers = {
            "X-RapidAPI-Key": os.environ["RAPID_API_KEY"],
            "X-RapidAPI-Host": "tasty.p.rapidapi.com",
        }
        res = requests.get(url, headers=headers, params=params)
        data = res.json()
        return data["results"]

    def get_one_by_recipe_id(self, id: int):
        url = "https://tasty.p.rapidapi.com/recipes/get-more-info"
        params = {"id": id}

        headers = {
            "X-RapidAPI-Key": os.environ["RAPID_API_KEY"],
            "X-RapidAPI-Host": "tasty.p.rapidapi.com",
        }
        res = requests.get(url, headers=headers, params=params)

        data = res.json()
        return data
