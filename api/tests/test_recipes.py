from fastapi.testclient import TestClient
from main import app
from queries.recipes import RecipesQueries


client = TestClient(app)


class FakeRecipeQueries:
    def list_recipes(self):
        return []

    def get_one_by_recipe_id(self, id: int):
        return {
            "name": "Gooey 'Box' Brownie Mud Hen Bars",
            "description": "A heavenly dessert that consists of a fudgy brownie base topped with a layer of gooey marshmallows and chocolate chips. The ultimate indulgence for any chocoholic.",
            "thumbnail_url": "https://img.buzzfeed.com/video-api-prod/assets/d904436b9c6a43cf8329edcd75768f5a/BFV4054_GooeyBrownieMudHenBars-Thumb1080.jpg",
            "user_ratings": {
                "count_positive": 73,
                "score": 0.858824,
                "count_negative": 12,
            },
            "credits": [{"name": "Tucker Iida"}],
            "instructions": [
                {
                    "position": 1,
                    "display_text": "Prepare Brownie Mix according to directions on the box.",
                },
                {
                    "position": 2,
                    "display_text": "In a medium mixing bowl add two egg whites and beat until stiff peaks form (about 2-3 minutes).",
                },
                {
                    "position": 3,
                    "display_text": "Carefully fold in Brown Sugar until incorporated.",
                },
                {
                    "position": 4,
                    "display_text": "Pour Brownie Mix into greased 9x9 inch (23x23 cm) baking pan.",
                },
                {
                    "position": 5,
                    "display_text": "Top Brownie Mix with Pecans, Marshmallows, and Chocolate Chips.",
                },
                {
                    "position": 6,
                    "display_text": "Spread Brown Sugar Meringue evenly over brownie layer.",
                },
                {
                    "position": 7,
                    "display_text": "Bake in preheated oven at 350˚F (175˚C) for 35 minutes. If the top starts to brown too quickly - cover loosely with aluminum foil halfway through.",
                },
                {
                    "position": 8,
                    "display_text": "Remove from oven and allow to cool completely at room temperature.",
                },
                {"position": 9, "display_text": "Enjoy!"},
            ],
        }


def test_list_recipes():
    app.dependency_overrides[RecipesQueries] = FakeRecipeQueries
    res = client.get("/api/recipes/")
    data = res.json()

    assert res.status_code == 200
    assert data == {"recipes": []}


def test_get_recipe_by_ids():
    app.dependency_overrides[RecipesQueries] = FakeRecipeQueries
    res = client.get("/api/recipes/7470/")
    data = res.json()

    assert res.status_code == 200
    assert data == {
        "name": "Gooey 'Box' Brownie Mud Hen Bars",
        "description": "A heavenly dessert that consists of a fudgy brownie base topped with a layer of gooey marshmallows and chocolate chips. The ultimate indulgence for any chocoholic.",
        "thumbnail_url": "https://img.buzzfeed.com/video-api-prod/assets/d904436b9c6a43cf8329edcd75768f5a/BFV4054_GooeyBrownieMudHenBars-Thumb1080.jpg",
        "user_ratings": {
            "count_positive": 73,
            "score": 0.858824,
            "count_negative": 12,
        },
        "credits": [{"name": "Tucker Iida"}],
        "instructions": [
            {
                "position": 1,
                "display_text": "Prepare Brownie Mix according to directions on the box.",
            },
            {
                "position": 2,
                "display_text": "In a medium mixing bowl add two egg whites and beat until stiff peaks form (about 2-3 minutes).",
            },
            {
                "position": 3,
                "display_text": "Carefully fold in Brown Sugar until incorporated.",
            },
            {
                "position": 4,
                "display_text": "Pour Brownie Mix into greased 9x9 inch (23x23 cm) baking pan.",
            },
            {
                "position": 5,
                "display_text": "Top Brownie Mix with Pecans, Marshmallows, and Chocolate Chips.",
            },
            {
                "position": 6,
                "display_text": "Spread Brown Sugar Meringue evenly over brownie layer.",
            },
            {
                "position": 7,
                "display_text": "Bake in preheated oven at 350˚F (175˚C) for 35 minutes. If the top starts to brown too quickly - cover loosely with aluminum foil halfway through.",
            },
            {
                "position": 8,
                "display_text": "Remove from oven and allow to cool completely at room temperature.",
            },
            {"position": 9, "display_text": "Enjoy!"},
        ],
    }
