# Savory Bites

- Samantha Alkaraisha
- Maria Perez
- Gabriel Lugo
- Erik Kordylasinski

# Design

[API design](apis.md)
[GHI](ghi.md)
[Data Models](DataModels)
[Integrations](integrations.md)

# Website purpose

We created this website for people to be able to view recipes based on ratings they've received & learn how to make them by navigating to their detailed page. Users can favorite recipes so that they may have easier access to them.

# Functionality

- Users would first need to sign-up & then proceed to login
- On the homepage,recipe data is coming in from a fast API & they can search for recipes or just scroll through the options.
- The recipes can all be clicked to view their detail page
- If a user likes a recipe, they can favorite it & it gets stored in their personal "my recipes" page
- They can delete their recipes from their favorites page
- They can activate dark mode if they choose on the homepage
- They can also navigate to an FAQ page & a contact us page. As well as the repo for the project.

# Initialization


- Users will be able to launch the app by cloning from this repository : https://gitlab.com/salkaraisha/savory-bites
- Once the repository is built they will need to create a docker volume database with the command "docker volume create recipes-mongo-data"
- When the database volume is finished creating, users can build their images and containers using the command "docker compose up --build" to create the virtual environments for users to view both the website and the Mongo database.
-Once the React server on port:3000 fully launches users will be able to fully view and interact with our recipe review site.
