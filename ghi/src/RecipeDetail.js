import { useParams } from 'react-router-dom';
import { useGetRecipeByIdQuery } from "./app/apiSlice";
import './style/RecipeDetail.css';
import React from 'react';
import animationData from './animations/Loadingdots.json';
import Lottie from 'lottie-react';

const RecipeDetail = () => {
    const { id } = useParams();
    const { data, isLoading } = useGetRecipeByIdQuery(id);



    const convertToRating = (score) => {
        if (score > 0.9) return 5;
        if (score === 0.9) return 4;
        if (score >= 0.8) return 4;
        if (score >= 0.7) return 3;
        if (score >= 0.6) return 3;
        if (score >= 0.5) return 2;
        if (score >= 0.4) return 2;
        return 1;
    };

    const renderStars = (rating) => {
        const filledStars = Math.floor(rating);
        const emptyStars = 5 - filledStars;

        const stars = [];

        for (let i = 0; i < filledStars; i++) {
            stars.push(<i key={i} className="bi bi-star-fill"></i>);
        }


        for (let i = 0; i < emptyStars; i++) {
            stars.push(<i key={i + filledStars} className="bi bi-star"></i>);
        }

        return stars;
    };

    if (isLoading) {
        return (
            <div className="loading-container">
                <Lottie
                    animationData={animationData}
                    loop={false}
                    autoplay={true}
                    style={{ width: '500px', height: '500px' }}
                />
            </div>
        );
    }


    return (
        <div>
            <div className="recipe-detail-card-container">
                <ul className="recipe-detail-card">
                    <li className="detail-recipe-name">
                        <h1>{data.name.toUpperCase()}</h1>
                    </li>
                    <li className='food-detail'>
                        <img src={data.thumbnail_url} height={500} width={500} alt="Food" />
                    </li>
                    <li className='detail-description'>
                        {data.description}
                    </li>
                    <li className="chef name">
                        Created By: {data.credits[0].name}
                    </li>
                    <li className="rating">
                        Rating: {renderStars(convertToRating(data.user_ratings.score))}
                    </li>
                    <li className="list-group-item">
                        Steps: {data.instructions.map((instruction) => {
                            return (
                                <div key={instruction.id}>
                                    ▫ {instruction.display_text}
                                </div>
                            )

                        })}
                    </li>
                </ul>
            </div>
        </div>
    );
};

export default RecipeDetail;
