import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';


export const recipeApi = createApi({
    reducerPath: 'recipeApi',
    baseQuery: fetchBaseQuery({
        baseUrl: process.env.REACT_APP_API_HOST,
        keepUnusedDataFor: 30 * 60 * 1000,
    }),
    endpoints: (builder) => ({
        getAllRecipes: builder.query({
            query: () => '/api/recipes',
            providesTags: [{ type: "Recipes", id: "LIST" }],
            staleTime: 30 * 60 * 1000,
        }),
        getRecipeById: builder.query({
            query: (recipe_id) => `/api/recipes/${recipe_id}`,
            providesTags: (recipe_id) => [{ type: "Recipe", id: recipe_id }]
        }),
        getFavorites: builder.query({
            query: () => ({
                url: `/api/favorites`,
                credentials: 'include'
            }),
            providesTags: ['Favorites'],
            transformResponse: (response) => response.favorites
        }),

        createFavorite: builder.mutation({
            query: (body) => ({
                url: `/api/favorites`,
                method: "POST",
                body,
                credentials: "include"
            }),
            invalidatesTags: ["Favorites"],
        }),
        deleteFavorite: builder.mutation({
            query: (favorite_id) => ({
                url: `/api/favorites/${favorite_id}`,
                method: "DELETE",
                credentials: "include"
            }),
            invalidatesTags: ["Favorites"],
        }),
        getAccount: builder.query({
            query: () => ({
                url: `/token`,
                credentials: 'include',
            }),
            transformResponse: (response) => response?.account || null,
            providesTags: ['Account'],
        }),
        logout: builder.mutation({
            query: () => ({
                url: `/token`,
                method: 'DELETE',
                credentials: 'include',
            }),
            invalidatesTags: ['Account'],
        }),
        login: builder.mutation({
            query: ({ username, password }) => {
                const body = new FormData();
                body.append('username', username);
                body.append('password', password);
                return {
                    url: `/token`,
                    method: 'POST',
                    body,
                    credentials: 'include',
                };
            },
            invalidatesTags: ['Account'],
        }),
        signup: builder.mutation({
            query: (body) => ({
                url: `/api/accounts`,
                method: 'POST',
                body,
                credentials: 'include',
            }),
            invalidatesTags: ['Account'],
        }),
    }),
});

export const {
    useGetAccountQuery,
    useGetRecipeByIdQuery,
    useGetAllRecipesQuery,
    useCreateFavoriteMutation,
    useDeleteFavoriteMutation,
    useGetFavoritesQuery,
    useLogoutMutation,
    useLoginMutation,
    useSignupMutation,
    useGetRecipeByIDQuery,
    usegetAllRecipesQuery,
} = recipeApi;
