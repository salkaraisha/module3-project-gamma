import React from 'react';
import { NavLink } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import './style/RecipeCard.css';

const RecipeCard = ({ recipeId, thumbnailUrl, recipeName, onDelete }) => {
    const handleDelete = () => {
        const shouldDelete = window.confirm('Are you sure you want to delete this recipe?');

        if (shouldDelete) {
            onDelete(recipeId);
        }
    };

    return (
        <div className="recipe-card">
            <NavLink to={`/recipes/${recipeId}`} style={{ textDecoration: 'none' }}>
                <h4 className="recipe-name">{recipeName}</h4>
            </NavLink>
            <div className="recipe-image-container">
                <img src={thumbnailUrl} alt={`Recipe ${recipeId}`} className="recipe-image" />
            </div>
            <div className="recipe-details">
                <button onClick={handleDelete} className="button">
                    <FontAwesomeIcon icon={faTrashAlt} /> Delete
                </button>
            </div>
        </div>
    );
};

export default RecipeCard;
