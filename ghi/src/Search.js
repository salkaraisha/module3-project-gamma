import { useState } from "react";

const Search = ({ onFilter, onReset }) => {
    const [searchCriteria, setSearchCriteria] = useState('');

    const handleKeyUp = (e) => {
        onFilter(e.target.value);
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        onFilter(searchCriteria);
    };

    const handleReset = () => {
        setSearchCriteria('');
        onReset();
    };

    return (
        <form className="row" onSubmit={handleSubmit}>
            <div className="col">
                <div className="input-group">
                    <input
                        className="form-control form-control-lg"
                        type="text"
                        placeholder="ie. chicken"
                        value={searchCriteria}
                        onChange={(e) => setSearchCriteria(e.target.value)}
                        onKeyUp={handleKeyUp}
                    />
                </div>
            </div>
        </form>
    );
};

export default Search;
