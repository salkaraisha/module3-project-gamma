import React, { useState, useEffect, useCallback } from 'react';
import Lottie from 'lottie-react';
import Search from './Search';
import { useDispatch } from 'react-redux';
import { reset } from './app/searchSlice';
import { Carousel } from 'react-bootstrap';
import day from './images/day.jpg';
import night from './images/night.jpg'
import { useCreateFavoriteMutation, useGetFavoritesQuery, useDeleteFavoriteMutation, useGetAccountQuery } from "./app/apiSlice"
import searchAnimationData from './animations/search.json';
import './style/ListRecipes.css';
import { NavLink } from 'react-router-dom';
import Loading from './Loading';


function ListRecipes() {
    const [recipes, setRecipes] = useState([]);
    const [searchTerm, setSearchTerm] = useState('');
    const [isDarkMode, setIsDarkMode] = useState(false);
    const [createFavoriteMutation] = useCreateFavoriteMutation();
    const { data: favorites } = useGetFavoritesQuery();
    const { data: accountData } = useGetAccountQuery();
    const [deleteFavoriteMutation] = useDeleteFavoriteMutation();
    const [isLoading, setLoading] = useState(true);
    const dispatch = useDispatch();
    const [showNotification, setShowNotification] = useState(false);
    const [showRemovedNotification, setShowRemovedNotification] = useState(false);

    useEffect(() => {
        getData();
    }, []);

    const getData = async () => {
        const response = await fetch('http://localhost:8000/api/recipes/');
        if (response.ok) {
            const data = await response.json();
            setRecipes(data.recipes);
            setLoading(false);
        }
    };

    useEffect(() => {
        if (favorites && favorites.length > 0) {
            const favoritedIds = favorites.map((favorite) => favorite.recipe_id);
            setFavoritedRecipeIds(favoritedIds);
        }
    }, [favorites]);

    const [favoritedRecipeIds, setFavoritedRecipeIds] = useState([]);

    const handleToggleFavorite = async (recipeId) => {
        if (favoritedRecipeIds.includes(recipeId)) {
            await deleteFavoriteMutation(recipeId);
            setFavoritedRecipeIds((prevState) => prevState.filter((id) => id !== recipeId));
            setShowRemovedNotification(true);
            setTimeout(() => {
                setShowRemovedNotification(false);
            }, 2000);
        } else {
            await createFavoriteMutation({ recipe_id: recipeId });
            setFavoritedRecipeIds((prevState) => [...prevState, recipeId]);
            setShowNotification(true);
            setTimeout(() => {
                setShowNotification(false);
            }, 2000);
        }
    };

    const filteredRecipes = recipes.filter((recipe) => {
        return recipe.name.toLowerCase().includes(searchTerm.toLowerCase());
    });

    const handleFilter = (searchCriteria) => {
        setSearchTerm(searchCriteria);
    };

    const handleReset = () => {
        dispatch(reset());
        setSearchTerm('');
    };

    const handleDarkModeToggle = () => {
        setIsDarkMode(!isDarkMode);
    };

    useEffect(() => {
        document.body.classList.toggle('dark-mode', isDarkMode);
    }, [isDarkMode]);

    const convertToRating = (score) => {
        if (score > 0.9) return 5;
        if (score === 0.9) return 4;
        if (score >= 0.8) return 4;
        if (score >= 0.7) return 3;
        if (score >= 0.6) return 3;
        if (score >= 0.5) return 2;
        if (score >= 0.4) return 2;
        return 1;
    };

    const renderStars = (rating) => {
        const filledStars = Math.floor(rating);
        const emptyStars = 5 - filledStars;

        const stars = [];

        for (let i = 0; i < filledStars; i++) {
            stars.push(<i key={i} className="bi bi-star-fill gold"></i>);
        }

        for (let i = 0; i < emptyStars; i++) {
            stars.push(<i key={i + filledStars} className="bi bi-star"></i>);
        }

        return stars;
    };


    const featuredRecipes = filteredRecipes
        .slice()
        .sort((a, b) => convertToRating(b.user_ratings.score) - convertToRating(a.user_ratings.score))
        .slice(0, 5);

    useEffect(() => {
        setTimeout(() => {
            setLoading(false);
        }, 3000);
    }, []);

    const [activeIndex, setActiveIndex] = useState(0)

    const handleNext = useCallback(() => {
        setActiveIndex((prevIndex) => (prevIndex + 1 % featuredRecipes.length) % featuredRecipes.length)
    }, [featuredRecipes.length])

    useEffect(() => {
        const interval = setInterval(() => {
            handleNext()
        }, 5000)
        return () => clearInterval(interval)
    }, [handleNext])


    const renderHeartIcon = (recipeId) => {
        const isLoggedIn = accountData !== undefined && accountData !== null;

        if (!isLoggedIn) {
            return null;
        }

        const isFavorited = favoritedRecipeIds.includes(recipeId);
        const heartClass = isFavorited ? "bi bi-heart-fill" : "bi bi-heart";
        const heartColor = isFavorited ? "red" : isDarkMode ? "red" : "black";

        return <i className={heartClass} style={{ color: heartColor }}></i>;
    };

    return (
        <div className={`container-fluid ${isDarkMode ? 'dark-mode' : ''}`}>
            {isLoading ? (
                <Loading />
            ) : (
                <div className="row align-items-center">
                    <div className="col-auto dark-mode-toggle-container">
                        <label htmlFor="darkModeToggle">
                            {isDarkMode ? <i className="bi bi-brightness-high-fill"></i> : <i className="bi bi-moon-stars-fill"></i>}
                        </label>
                        <div className="form-check form-switch">
                            <input
                                className="form-check-input"
                                type="checkbox"
                                id="darkModeToggle"
                                checked={isDarkMode}
                                onChange={handleDarkModeToggle}
                            />
                        </div>
                    </div>
                    <div className={`searchbar`}>
                        <div className="banner">
                            <img className="banner-image" src={isDarkMode ? night : day} alt={isDarkMode ? "night" : "day"} />
                        </div>
                        <section>
                            <div className="featured-section">
                                <h1>Featured</h1>
                                <p>Top rated recipes</p>
                                <i className="bi bi-star-fill gold"></i>
                                <i className="bi bi-star-fill gold"></i>
                                <i className="bi bi-star-fill gold"></i>
                                <i className="bi bi-star-fill gold"></i>
                                <i className="bi bi-star-fill gold"></i>
                            </div>
                            <div className="recipe-cards-container">
                                <Carousel activeIndex={activeIndex} onSelect={(index) => setActiveIndex(index)}>
                                    {featuredRecipes.map((recipe, index) => (
                                        <Carousel.Item key={recipe.id}>
                                            <NavLink to={`/recipes/${recipe.id}`}>
                                                <img
                                                    className="d-block w-100 carousel-image"
                                                    src={recipe.thumbnail_url}
                                                    alt={`Recipe ${recipe.id}`}
                                                />
                                            </NavLink>
                                            <Carousel.Caption className="carousel-caption">
                                                <h5>{recipe.name}</h5>
                                            </Carousel.Caption>
                                        </Carousel.Item>
                                    ))}
                                </Carousel>
                            </div>
                        </section>
                    </div>

                    <div className="searchTitle">
                        <h1>Unleash your creativity</h1>
                        <p>Explore recipes</p>
                        <Search onFilter={handleFilter} onReset={handleReset} />
                    </div>

                    <div className="lottie-container">
                        <Lottie animationData={searchAnimationData} loop={true} autoplay={true} />
                    </div>
                    <div className="recipe-cards-container">
                        {filteredRecipes.map((recipe) => (
                            <div key={recipe.id} className="recipe-card">
                                <h4 className="recipe-name">
                                    <NavLink to={`/recipes/${recipe.id}`}>
                                        {recipe.name}
                                    </NavLink>
                                </h4>
                                <img src={recipe.thumbnail_url} alt={`Recipe ${recipe.id}`} className="recipe-image" />
                                <div className="recipe-details">
                                    <p className="chef-name">Chef: {recipe.credits[0].name}</p>
                                    <p className="ratings">
                                        Ratings: {renderStars(convertToRating(recipe.user_ratings.score))}
                                    </p>
                                    {renderHeartIcon(recipe.id) && (
                                        <button
                                            onClick={() => handleToggleFavorite(recipe.id)}
                                            className="button-heart"
                                            style={{ borderColor: 'red' }}
                                        >
                                            {renderHeartIcon(recipe.id)}
                                        </button>
                                    )}
                                </div>
                            </div>
                        ))}
                    </div>
                    {showNotification && (
                        <div className="notification">
                            Added to favorites!
                        </div>
                    )}
                    {showRemovedNotification && (
                        <div className="notification removed">
                            Removed from favorites!
                        </div>
                    )}
                    <section className="top-0 w-full">
                        <footer className="footer footer-center p-10 bg-base-300 text-base-content">
                            <div>
                                <p>Copyright © 2023 - All rights reserved by Tasty Industries LLC</p>
                            </div>
                        </footer>
                    </section>
                </div>
            )}

        </div>
    );
}
export default ListRecipes;
