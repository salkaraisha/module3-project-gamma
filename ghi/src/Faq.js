import React from 'react';
import "./style/Faq.css"
import womanAnimation from './animations/woman.json';
import Lottie from 'lottie-react';


const WomanAnimation = () => {
    return (
        <div>
            <Lottie animationData={womanAnimation} />
        </div>
    );
};

const Faq = () => {

    return (
        <div className="faq-container">
            <div className="faq-content">
                <div className="faq-h1">
                    <h1>Frequently Asked Questions</h1>
                </div>
                <div className="faq-p">
                    <p>
                        Welcome to our FAQ page! Here, we have compiled a list of common questions and their answers to provide you with helpful information about our product/service. If you have any additional questions that are not covered below, please feel free to contact us, and we'll be happy to assist you.
                    </p>
                </div>
            </div>
            <div className='faq-lottie-container'>
                <WomanAnimation />
            </div>
            <div className="faq-list">
                <div className="faq-item">
                    <h4>How do I properly season food?</h4>
                    <p>Proper seasoning involves using a combination of salt, pepper, herbs, and spices to enhance the flavors of your dish. Start with a small amount, taste, and adjust as needed. Remember, it's easier to add more seasoning than to remove excess.</p>
                </div>
                <div className="faq-item">
                    <h4>How can I improve my knife skills?</h4>
                    <p>Practice and technique are key to improving knife skills. Hold the knife with a firm grip, use a proper cutting board, and practice slicing, dicing, and chopping various ingredients. It's also helpful to keep your knife sharp for better control.</p>
                </div>
                <div className="faq-item">
                    <h4>How can I prevent food from sticking to the pan?</h4>
                    <p>To prevent food from sticking, make sure the pan is properly heated before adding ingredients. Use an adequate amount of cooking oil or butter, and avoid overcrowding the pan. Allow the food to develop a crust before attempting to flip or move it.</p>
                </div>
                <div className="faq-item">
                    <h4>How do I know when meat is cooked to the right temperature?</h4>
                    <p>The best way to ensure meat is cooked to the correct temperature is by using a meat thermometer. Different meats have different safe internal temperatures. Insert the thermometer into the thickest part of the meat, away from bones, and refer to a temperature guide for the specific meat you're cooking.</p>
                </div>
                <div className="faq-item">
                    <h4>What are some essential kitchen tools every cook should have?</h4>
                    <p>Essential kitchen tools include a chef's knife, cutting board, measuring cups and spoons, mixing bowls, a skillet or frying pan, a saucepan, a baking sheet, and basic utensils like spatulas and tongs.</p>
                </div>
                <div className="faq-item">
                    <h4>How can I make my baked goods rise properly?</h4>
                    <p>To help baked goods rise properly, make sure to use fresh baking powder or yeast, as these are responsible for the leavening process. Follow the recipe's instructions regarding mixing and resting times, and avoid opening the oven door too often while baking.</p>
                </div>
                <div className="faq-item">
                    <h4>How can I make a creamy sauce without using heavy cream?</h4>
                    <p>You can achieve a creamy sauce without heavy cream by using alternatives like milk, yogurt, coconut milk, or a combination of milk and cornstarch to thicken the sauce. Experiment with different options to find the taste and consistency you prefer.</p>
                </div>
                <div className="faq-item">
                    <h4>How do I prevent my vegetables from becoming mushy when cooking?</h4>
                    <p>To prevent vegetables from becoming mushy, avoid overcooking them. Cook them until they are tender but still have a slight bite. You can also try blanching them in boiling water for a short time and then transferring them to an ice bath to stop the cooking process.</p>
                </div>
                <div className="faq-item">
                    <h4>How can I make homemade stocks or broths?</h4>
                    <p>Homemade stocks or broths can be made by simmering bones (such as chicken or beef), vegetables, and aromatics (like onions, carrots, and herbs) in water for an extended period. This slow cooking process extracts flavors and creates a flavorful base for soups, stews, and sauces.</p>
                </div>
                <div className="faq-item">
                    <h4>How do I properly store leftovers?</h4>
                    <p>To store leftovers properly, allow them to cool down before transferring them to airtight containers or wrap them tightly with plastic wrap. Label and date the containers, and store them in the refrigerator for a few days or in the freezer for longer-term storage.</p>
                </div>
                <div className="faq-item">
                    <h4>Can I use dairy-free milk in place of regular milk?</h4>
                    <p>Yes, you can often use dairy-free milk alternatives like almond, soy, or cashew milk as a substitute for regular milk in many recipes.</p>
                </div>
                <div className="faq-item">
                    <h4>Can I make this recipe vegan?</h4>
                    <p>The possibility of making a recipe vegan depends on the specific ingredients and techniques used. Some recipes can be easily veganized by substituting animal-based ingredients with plant-based alternatives, while others may require more experimentation.</p>
                </div>
            </div>
        </div>
    );
};
export default Faq;
