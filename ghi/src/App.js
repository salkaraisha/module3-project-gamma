import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { AuthProvider } from "@galvanize-inc/jwtdown-for-react";
import Nav from './Nav';
import LoginForm from './LoginForm';
import SignupForm from './SignupForm';
import ListRecipes from './ListRecipes';
import About from './About';
import Faq from './Faq';
import FavoriteRecipes from './FavoriteRecipes';
import RecipeDetail from './RecipeDetail';

function App(props) {
  return (
    <div className="container-fluid">
      <BrowserRouter>
        <AuthProvider>
          <Nav />
          <Routes>
            <Route path="/" element={<ListRecipes />}></Route>
            <Route exact path="/signup" element={<SignupForm />}></Route>
            <Route exact path="/login" element={<LoginForm />}></Route>
            <Route exact path="/recipes/:id" element={<RecipeDetail />}></Route>
            <Route exact path="/favorites/mine" element={<FavoriteRecipes />}></Route>
            <Route exact path="/about" element={<About />}></Route>
            <Route exact path="/faq" element={<Faq />}></Route>
          </Routes>
        </AuthProvider>
      </BrowserRouter>
    </div>
  );
}

export default App;
